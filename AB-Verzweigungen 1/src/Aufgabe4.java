import java.util.Scanner;

public class Aufgabe4 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		int zahl1;
		
		System.out.print("Gebe eine Zahl zwischen 0 und 100 ein:");
		zahl1 = tastatur.nextInt();
		
		
		if (zahl1 <0 || zahl1 >100) {
			System.out.println("Fehlerhafte Eingabe");
		}else if (zahl1 >50){
			System.out.println("Gro�");
		}else if (zahl1 <50)
			System.out.println("Klein");
	}

}
