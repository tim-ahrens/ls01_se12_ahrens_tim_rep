
/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 150000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int  bewohnerBerlin = 364500000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 7743;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm =   1500000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int  flaecheGroessteLand = 17098242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double  flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzhal der Bewohner von Berlin: " + bewohnerBerlin );
    
    System.out.println("Mein Alter in Tagen: " + alterTage );
    
    System.out.println("Gewicht des schwersten Tiers in kg: " + gewichtKilogramm );
    
    System.out.println("Fl�che des gr��ten Landes der Welt in km^2: " + flaecheGroessteLand );
    
    System.out.println("Das kleinste Land hat eine Fl�che von " + flaecheKleinsteLand + " km^2." );
    
    System.out.println(" ");
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
