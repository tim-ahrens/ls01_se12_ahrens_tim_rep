//Bearbeitet wurde hier das Arbeitsblatt "Arrays Klausurvorbereitung" (A.2,4,6,7,8)
//und die Aufgabe "Vorbereitung KA4" direkt aus dem Kurs LF05
import java.util.Scanner;
public class ArraysKlausurvorbereitung {

	public static void main(String[] args) {
		
		// AB/2
		double [] meinArray = new double [5];
		
		// AB/4
		meinArray[2] = 99.9;
		meinArray[3] = 100.5;
		
		// AB/6 
		for (int i = 0; i < meinArray.length; i++){
		   System.out.println(meinArray[i]);
		}
		
		System.out.println("");
		
		// AB/7
		double [] meinArray2 = new double [7];
		int wert = 1;
		
		for (int i = 0; i < meinArray2.length; i++){
			meinArray2[i] = wert++ * 10;
			
			System.out.println(meinArray2[i]);
		}
		
		System.out.println("");
		
		// AB/8 - Zusatz:
		double temp;
		
		temp = meinArray2[2];
		meinArray2[2] = meinArray2[4];
		meinArray2[4] = temp;
		
		for (int i = 0; i < meinArray2.length; i++) {
			System.out.println(meinArray2[i]);
		}
		
		//Vorbereitung KA4:
		
		int arr [] = new int [10]; //Deklarieren des Arrays mit 10 Werte
		Scanner sc = new Scanner(System.in);
		
		System.out.println(" \nGib 10 Werte ein:"); //Beliebige Werteingabe durch Nutzer
		for (int i = 0; i < arr.length;i++) {
			arr[i] = sc.nextInt();
		}
		
		for (int i = 0; i < arr.length;i++) { //Multiplikation aller Werte im Array mit 100
			arr[i] = arr[i]*100;
			System.out.println(arr[i]);
		}
		System.out.println("");
		
		for (int i = 0; i < arr.length;i++) { //Addieren bei Index 0 mit 5, 1 mit 10, usw.
			int zahl = (i + 1) * 5;
			arr[i] = arr[i] + zahl;
		}
		
		System.out.println("");
		
		for (int i = arr.length -1 ; i >= 0 ;i--) { // Ausgabe der Arraywerte von hinten nach vorne
			
			System.out.println(arr[i]);
		}
		
		System.out.println("");
		
		int temp2; // Tausch der Elemente von 1. und letzter Indexposition
		temp2 = arr[arr.length -1];
		arr[arr.length -1] = arr[0];
		arr[0] = temp2;
		
		for (int i = 0; i < arr.length ;i++) {
			
			System.out.println(arr[i]);
		}
		
		
 		
		
	}

}
