import java.util.Scanner;

public class Makler {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double meter1 ; 
		double meter2 ;
		
		System.out.println("L�nge (m): ");
		meter1 = sc.nextDouble();
		System.out.println("Breite (m): ");
		meter2 = sc.nextDouble();

		double ergebnis = meter1 * meter2;
		
		System.out.println("Das Zimmer hat " + ergebnis + " qm.");
		
	}

}
