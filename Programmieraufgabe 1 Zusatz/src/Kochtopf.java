import java.util.Scanner;

public class Kochtopf {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double td; 
		double th;
		double ccm;
		final double PI = 3.1415926535897932385;
		double radius;
		double ergebnis;
		
		System.out.printf("%-27s : " , "Topfdurchmesser in cm ");
		td = sc.nextDouble();
		System.out.printf("%-27s : ","Topfh�he in cm ");
		th = sc.nextDouble();
		
		radius = td / 2; 
		ccm = PI * (radius * radius) * th;
		ergebnis = ccm / 1000;
		
		
		System.out.printf("Das Kochtopfvolumen umfasst : %.2f Liter", ergebnis);
		

	}

}
