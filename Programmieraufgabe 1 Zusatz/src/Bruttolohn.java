import java.util.Scanner;

public class Bruttolohn {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double mA; 
		double sL;
		double mB;
		double jB;
		
		System.out.printf("%-50s: ", "Monatliche Arbeitszeit in Stunden");
		mA = sc.nextDouble();
		System.out.printf("%-50s: ","Stundenlohn in Euro ");
		sL = sc.nextDouble();
		
		mB = mA * sL ;
		jB= mA * sL * 12 ;
		
		System.out.printf("\n%-50s: %.2f", "Der monatliche Bruttolohn in Euro betr�gt", mB);
		System.out.printf("\nDer j�hrliche Bruttolohn in Euro betr�gt : %.2f", jB);
	}

}
