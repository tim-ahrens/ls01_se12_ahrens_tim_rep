import java.util.Scanner;

public class Benzinrechner {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double km1; 
		double km2;
		double verbrauch;
		double ergebnis;
		
		System.out.print("km Stand beim ersten Tanken: ");
		km1 = sc.nextDouble();
		System.out.printf("%-26s : ","km Stand beim Tanken ");
		km2 = sc.nextDouble();
		System.out.printf("%-4s : " , "Benzinverbrauch in Litern ");
		verbrauch = sc.nextDouble();
		
		ergebnis = (verbrauch * 100) / (km2 - km1);
		
		
		System.out.printf("\nDer PKW hat %.2f auf 100 Kilometer verbraucht.", ergebnis);

	}

}
