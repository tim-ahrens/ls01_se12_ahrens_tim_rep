import java.util.Scanner;

public class Investor {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double ez; 
		double zins;
		double jahre;
		double ergebnis;
		
		System.out.println("Einzahlung in Euro: ");
		ez = sc.nextDouble();
		System.out.println("Zinsen in %: ");
		zins = sc.nextDouble();
		System.out.println("Jahre: ");
		jahre = sc.nextDouble();

		ergebnis = (ez * zins) /100 * jahre ;
		
		System.out.println("Der Investor hat ingesamt " + ergebnis + " Euro erhalten.");

	}

}
