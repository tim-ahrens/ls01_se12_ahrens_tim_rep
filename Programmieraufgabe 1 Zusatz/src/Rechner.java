import java.util.Scanner;

public class Rechner {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double zahl1 ; 
		double zahl2 ;

		System.out.println("Wie lautet die erste Zahl ? ");
		zahl1 = sc.nextInt();
		System.out.println("Wie lautet die zweite Zahl ? ");
		zahl2 = sc.nextInt();
		
		System.out.println( " " );
		
		double ergebnis = zahl1 + zahl2;
		double ergebnis2 = zahl1 - zahl2;
		double ergebnis3 = zahl1 * zahl2;
		double ergebnis4 = zahl1 / zahl2;
		
		System.out.println( "Addition" );
		System.out.println( zahl1 + " + " + zahl2 + " = " + ergebnis );
		System.out.println( "Subbtraktion" );
		System.out.println( zahl1 + " - " + zahl2 + " = " + ergebnis2 );
		System.out.println( "Multiplikation" );
		System.out.println( zahl1 + " * " + zahl2 + " = " + ergebnis3 );
		System.out.println( "Division" );
		System.out.printf( zahl1 + " / " + zahl2 + " = %.2f" , ergebnis4 );
		System.out.println( " " );	

	}

}
