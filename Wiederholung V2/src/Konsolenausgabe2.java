
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
	//Aufgabe 1:
		System.out.println("Aufgabe 1:");
		System.out.println(" ");
		
		System.out.printf("%5s\n","**");
		System.out.printf("%1s\n","*      *");
		System.out.printf("%1s\n","*      *");
		System.out.printf("%5s\n","**");

		
	//Aufgabe 2:
		System.out.println("Aufgabe 2:");
		System.out.println(" ");
		
		
		System.out.printf("%-5s","0!");
		System.out.print("=");
		System.out.printf("%-19s"," ");
		System.out.print("=");
		System.out.printf("%4s\n","1");
		
		System.out.printf("%-5s","1!");
		System.out.print("=");
		System.out.printf("%-19s"," 1");
		System.out.print("=");
		System.out.printf("%4s\n","1");
		
		System.out.printf("%-5s","2!");
		System.out.print("=");
		System.out.printf("%-19s"," 1 * 2");
		System.out.print("=");
		System.out.printf("%4s\n","2");
		
		System.out.printf("%-5s","3!");
		System.out.printf("=");
		System.out.printf("%-19s"," 1 * 2 * 3");
		System.out.print("=");
		System.out.printf("%4s\n","6");
		
		System.out.printf("%-5s","4!");
		System.out.printf("=");
		System.out.printf("%-19s"," 1 * 2 * 3 * 4");
		System.out.print("=");
		System.out.printf("%4s\n","24");
		
		System.out.printf("%-5s","5!");
		System.out.printf("=");
		System.out.printf("%-19s"," 1 * 2 * 3 * 4 * 5");
		System.out.print("=");
		System.out.printf("%4s\n","120");
		
		
	//Aufgabe 3:
		System.out.println(" ");
		System.out.println("Aufgabe 3:");
		System.out.println(" ");
		
		System.out.printf("%-12s","Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s\n","Celsius");
		
		System.out.println("-----------------------");
		
		System.out.printf("%-12s","-20");
		System.out.print("|");
		System.out.printf("%10s\n","-28.8889");
		
		System.out.printf("%-12s","-10");
		System.out.print("|");
		System.out.printf("%10s\n","-23.3333");
		
		System.out.printf("%-12s","0");
		System.out.print("|");
		System.out.printf("%10s\n","-17.7778");
		
		System.out.printf("%-12s","20");
		System.out.print("|");
		System.out.printf("%10s\n","-6.6667");
	
		System.out.printf("%-12s","30");
		System.out.print("|");
		System.out.printf("%11s\n","-1.1111 ");
		
		
		System.out.print("");
		
	}

}
