// AUFGABE  1:

public class Aufgabe1 {

	public static void main(String[] args) {
		
		
		
		int a= 1;
		while (a<101) {
		System.out.print(a + " ");
		if (a%2 == 0) {
			System.out.println(" - Durch 2 teilbar\n");
		}else {
			System.out.println(" - Nicht durch 2 teilbar\n");
		}
		a++;
		if (a==101) continue;
		}
		
		
	}
}