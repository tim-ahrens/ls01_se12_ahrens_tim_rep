import java.util.Scanner;
public class Aufgabe4 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int a;
		int ergebnis=0;
		
		System.out.print("Gebe eine Zahl ein:");
		a = sc.nextInt();
 
		for (int i=1;i <= a;i++){
			System.out.print(i);
			if (i<a) {
				System.out.print(" + ");	
			}else if (i==a) {
				System.out.print(" = ");
			}
			ergebnis +=i;
		}
		System.out.println(ergebnis);
	 }

}