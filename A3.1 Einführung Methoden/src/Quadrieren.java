public class Quadrieren {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		double x = 5;
				
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis= 0;
		
		ergebnis = Quadrieren.zahlenQuadrieren (x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}	
	
		public static double zahlenQuadrieren (double a) {
			double b = a * a;
			return b;
	}
}
