
import java.util.Scanner;

class Addition {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        double zahl1 = 0.0, zahl2 = 0.0, erg;

        //1.Programmhinweis
        System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
           
        //4.Eingabe
        System.out.print(" 1. Zahl:");
        zahl1 = sc.nextDouble();
        System.out.print(" 2. Zahl:");
        zahl2 = sc.nextDouble();

        //3.Verarbeitung
       
        erg = Addition.zahlenAddition (zahl1, zahl2);

        //2.Ausgabe
        
        System.out.println("\nErgebnis der Addition:");
        System.out.printf("%.2f = %.2f + %.2f", erg, zahl1, zahl2);
    
        }
    
    
    public static double zahlenAddition (double zahl1, double zahl2) {
			double erg = zahl1 + zahl2 ;
			return erg;
	}
}