
import java.util.Scanner;

class Multiplikation {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        double zahl1 = 0.0, zahl2 = 0.0, erg;

        //1.Programmhinweis
        System.out.printf("Hinweis: \n\n");
        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");

        //4.Eingabe
        System.out.print(" 1. Zahl:");
        zahl1 = sc.nextDouble();
        System.out.print(" 2. Zahl:");
        zahl2 = sc.nextDouble();
        
        System.out.println("\nBitte warten");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		
 		}
        }

        //3.Verarbeitung
       
        erg = Multiplikation.zahlenMultiplikation (zahl1, zahl2);

        //2.Ausgabe
        System.out.println("\nErgebnis der Multiplikation:");
        System.out.printf("%.2f = %.2f * %.2f", erg, zahl1, zahl2);
    
    }
    
    public static double zahlenMultiplikation (double zahl1, double zahl2) {
			double erg = zahl1 * zahl2 ;
			return erg;
	}
}