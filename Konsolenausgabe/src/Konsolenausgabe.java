
public class Konsolenausgabe {

	public static void main(String[] args) {
	 
		String wort1;
		String wort2;
		
		wort1 = "Das";
		wort2 = "Beispielsatz";
		
		
//Aufgabe 1:
		System.out.println("");
		System.out.println("Aufgabe 1:");
		
		System.out.print("Das ist kein Beispielsatz =)");
		System.out.println("Kein Beispielsatz ist das =) ");
		
		
		System.out.print("\"Das ist kein Beispielsatz =)\"\n\n");
		System.out.println("Kein Beispielsatz ist das =) ");
		
		
		System.out.println(wort1 + " ist " + wort2 + " =)");
		System.out.println("Kein " + wort2 + " ist " + wort1);
		
		
		
		//print() gibt den Text aus und println() gibt den Text aus und f�hrt ein Zeilenumbruch ein
		
		
//Aufgabe 2:
		
		//Aufgrund der �bersicht deklariere und implimentiere ich zu jeder Aufgabe einzln.
		System.out.println("");
		System.out.println("Aufgabe 2:");
		
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.printf("%7s", "*\n");	
		System.out.printf("%8s", "***\n");
		System.out.printf("%9s", "*****\n");	
		System.out.printf("%10s", "*******\n");
		System.out.printf("%11s", "*********\n");
		System.out.printf("%12s", "***********\n");
		System.out.printf("%8s", "***\n");
		System.out.printf("%8s", "***\n");
		
//Aufgabe 3:		
		
		System.out.println("");
		System.out.println("Aufgabe 3:");
		
		double a, b, c, d, e;
		
		a = 22.4234234;
		b = 111.2222;
		c = 4.0;
		d = 1000000.551;
		e = 97.34;
		
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
				
		
	}

}
